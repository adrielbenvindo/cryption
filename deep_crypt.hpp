void deep_encryption(char* file_path1, char* file_path2){
  FILE* file = fopen(file_path1,"r");
  ListcC* lines = NULL;
  ListcC* data_lines = NULL;
  if(file!=NULL){
    char character;
    int last=0;
    int next;
    char number[4];
    char* hole = (char*) malloc(0);
    lines = insertC(lines,hole);
    data_lines = lines;
    while(!feof(file)){
      character = fgetc(file);
      if(character != '\n'){
        if(character != ' ' && character != EOF){
          next = (int) character;
          next*=2;
          next+=4+last/2;
          next-=25*(next/100);
          snprintf(number,4,"%c",next);
          strcat(data_lines->information,number);
          last = next;
        }else if(character == ' '){
          strcat(data_lines->information," ");
        }
      }else{
        insertC(lines,hole);
        data_lines = data_lines->next;
      }
    }
    fclose(file);
  }else
    perror("failed to open file");
  file = fopen(file_path2,"w");
  if(file != NULL){
    data_lines = data_lines->next;
    fprintf(file,"[dc]\n");
    do{
      if(strcmp(data_lines->information,"") != 0){
        fprintf(file,"%s\n",data_lines->information);
      }
      data_lines = data_lines->next;
    }while(data_lines != lines);
    fclose(file);
  }else
    perror("failed to open file");
}

void deep_decription(char* file_path1, char* file_path2){
  FILE* file = fopen(file_path1,"r");
  ListcC* lines = NULL;
  ListcC* data_lines = NULL;
  if(file!=NULL){
    char character;
    int last=0;
    int next;
    int next_crypt;
    char number[4];
    char* hole = (char*) malloc(0);
    lines = insertC(lines,hole);
    data_lines = lines;
    if(!feof(file)){
      char init[5];
      fgets(init,5,file);
      fgetc(file);
    }
    while(!feof(file)){
      character = fgetc(file);
      if(character != '\n'){
        if(character != ' ' && character != EOF){
          next = (int) character;
          if(next < 0){
            next*=-1;
            
          }
          next_crypt = next;
          next+=25*(next/100);
          next-=4+last/2;
          next/=2;
          snprintf(number,4,"%c",next);
          strcat(data_lines->information,number);
          last = next_crypt;
        }else if(character == ' '){
          strcat(data_lines->information," ");
        }
      }else{
        insertC(lines,hole);
        data_lines = data_lines->next;
      }
    }
    fclose(file);
  }else
    perror("failed to open file");
  file = fopen(file_path2,"w");
  if(file != NULL){
    data_lines = data_lines->next;
    do{
      if(strcmp(data_lines->information,"") != 0){
        fprintf(file,"%s\n",data_lines->information);
      }
      data_lines = data_lines->next;
    }while(data_lines != lines);
    fclose(file);
  }else
    perror("failed to open file");
}

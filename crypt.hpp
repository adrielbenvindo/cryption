const char replacement_table[2][88] = {{'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','t','u','v','w','x','y','z','0','1','2','3','4','5','6','7','8','9','!','@','#','$','%','&','*','(',')','-','_','=','+','[',']','{','}',';',':',',','.','<','>','?','/','|'},{'C','!','h','1','w',';','i','L','/','B','5','-','q','T','m','n','#','J','F','l','z','|','O','3','A','{','}','U','j','_','W','p','9','X','t','%','E','Z','<','>','o','7','*','K','v','e','[',']','0','D','$','u','H',',','G','k','+','a','N','8','&','g','V','?','c','M','=','y','Y','4','(',')','b','I','.','2','f','Q',':','d','P','x','@','6'}};

void encryption(char* file_path1, char* file_path2){
  FILE* file = fopen(file_path1,"r");
  ListcC* lines = NULL;
  ListcC* data_lines = NULL;
  if(file!=NULL){
    char character;
    char new_character = ' ';
    int last=0;
    char* hole = (char*) malloc(0);
    lines = insertC(lines,hole);
    data_lines = lines;
    while(!feof(file)){
      character = fgetc(file);
      if(character != '\n'){
        if(character != ' ' && character != EOF){
          for(int numd=0;numd<88;numd++){
            if(replacement_table[0][numd] == character){
              if(numd + 6 + last < 88){
                new_character = replacement_table[1][numd + 6 + last];
                last = numd+6+last;
              }else{
                new_character = replacement_table[1][numd + 6 + last - 88];
                last = numd+6+last-88;
              }
              break;
            }
          }
          if(new_character != ' '){
            char string_character[2]={new_character,'\0'};
            strcat(data_lines->information,string_character);
          }else{
            char string_character[2]={character,'\0'};
            strcat(data_lines->information,string_character);
          }
          new_character = ' ';
        }else if(character == ' '){
          strcat(data_lines->information," ");
        }
      }else{
        lines = insertC(lines,hole);
        data_lines = data_lines->next;
      }
    }
    fclose(file);
  }else
    perror("failed to open file");
  file = fopen(file_path2,"w");
  if(file != NULL){
    fprintf(file,"[c]\n");
    data_lines = data_lines->next;
    do{
      if(strcmp(data_lines->information,"") != 0){
        fprintf(file,"%s\n",data_lines->information);
      }
      data_lines = data_lines->next;
    }while(data_lines != lines);
    fclose(file);
  }else
    perror("failed to open file");
}

void decription(char* file_path1, char* file_path2){
  FILE* file = fopen(file_path1,"r");
  ListcC* lines = NULL;
  ListcC* data_lines = NULL;
  if(file!=NULL){
    char character;
    char first_character;
    char old_character = ' ';
    int last=0;
    char hole[2];
    strcpy(hole,"");
    lines = insertC(lines,hole);
    data_lines = lines;
    if(!feof(file)){
      char init[5];
      fgets(init,4,file);
      fgetc(file);
      first_character = fgetc(file);
    }
    while(!feof(file)){
      if(first_character != '\n'){
        character = first_character;
        first_character = '\n';
      }else
        character = fgetc(file);
      if(character != '\n'){
        if(character != ' ' && character != EOF){
          for(int numd=0;numd<88;numd++){
            if(replacement_table[1][numd] == character){
              if(numd - 6 - last >= 0){
                old_character = replacement_table[0][numd - 6 - last];
              }else{
                old_character = replacement_table[0][numd - 6 - last + 88];
              }
              last = numd;
              break;
            }
          }
          if(old_character != ' '){
            char string_character[2]={old_character,'\0'};
            strcat(data_lines->information,string_character);
          }else{
            char string_character[2]={character,'\0'};
            strcat(data_lines->information,string_character);
          }
          old_character = ' ';
        }else if(character == ' '){
          strcat(data_lines->information," ");
        }
      }else{
        lines = insertC(lines,hole);
        data_lines = data_lines->next;
      }
    }
    fclose(file);
  }else
    perror("failed to open file");
  file = fopen(file_path2,"w");
  if(file != NULL){
    data_lines = data_lines->next;
    do{
      if(strcmp(data_lines->information,"") != 0){
        fprintf(file,"%s\n",data_lines->information);
      }
      data_lines = data_lines->next;
    }while(data_lines != lines);
    fclose(file);
  }else
    perror("failed to open file");
}

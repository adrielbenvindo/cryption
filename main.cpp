#include <iostream>
#include <cstring>
#include "simplec_circle_list.hpp"
#include "crypt.hpp"
#include "deep_crypt.hpp"

using namespace std;

void execution_show();
void execution_encryption();
void execution_decription();
void execution_encryption_args(char* type,char* file_path1,char* file_path2);
void execution_decription_args(char* file_path1,char* file_path2);

void show_content(char* file_path);

int main(int argc, char** argv){
  if(argc>1){
    if(strcmp(argv[1],"show") == 0){
      show_content(argv[2]);
    }else if(strcmp(argv[1],"crypt") == 0){
      if(argc==5)
        execution_encryption_args(argv[2],argv[3],argv[4]);
      else
        cout<<"lack or excess of arguments, try 'cryption help'";
    }else if(strcmp(argv[1],"decript") == 0){
      if(argc==4)
        execution_decription_args(argv[2],argv[3]);
      else
        cout<<"lack or excess of arguments, try 'cryption help'";
    }else if(strcmp(argv[1],"help") == 0){
      if(argc==2){
        cout<<"cryption [option] [crypt option] [FILES ...]"<<endl<<endl;
        cout<<"encryption and decription of text files"<<endl<<endl;
        cout<<"Options: "<<endl;
        cout<<"show - show content of one file"<<endl;
        cout<<"crypt - encrypt one file writing in other file"<<endl;
        cout<<"decript - decript one file writing in other file"<<endl;
        cout<<"help - show helper"<<endl<<endl;
        cout<<"crypt options: "<<endl;
        cout<<"options used only to encrypt files"<<endl;
        cout<<"-n or --normal - normal encryption using replacement table"<<endl;
        cout<<"-d or --deep - deep encryption manipulate character as numbers"<<endl;
      }else
        cout<<"excess of arguments, try 'cryption help'";
    }else
      cout<<"invalid option '"<<argv[1]<<"', try 'cryption help'";
  }else{
    int option;
    cout<<"Option: ";
    cout<<endl<<"0 - exit";
    cout<<endl<<"1 - show content";
    cout<<endl<<"2 - encryption of file";
    cout<<endl<<"3 - decription of file";
    cout<<endl<<"choose: ";
    cin>>option;
    system("cls");
    switch(option){
      case 0:
        exit(0);
        break;
      case 1:
        execution_show();
        break;
      case 2:
        execution_encryption();
        break;
      case 3:
        execution_decription();
        break;
      default:
        cout<<"invalid option"<<endl;
        system("pause");
        system("cls");
        char** argv;
        strcpy(argv[0],"cryption");
        main(1,argv);
    }
    char answer;
    cout<<"execute any? [Y/n] ";
    cin>>answer;
    system("cls");
    if(answer=='Y'){
      char** argv = 0;
      main(1,argv);
    }
  }
}

void execution_show(){
  char* file_path = (char*) malloc(200);
  cout<<"input the file path: ";
  cin>>file_path;
  system("cls");
  show_content(file_path);
}

void execution_encryption(){
  char* file_path1 = (char*) malloc(200);
  cout<<"input the file path to encryption: ";
  cin>>file_path1;
  system("cls");
  char* file_path2 = (char*) malloc(200);
  cout<<"input the file path to write: ";
  cin>>file_path2;
  system("cls");
  char type;
  cout<<"normal encryption or deep encryption? [n/d] ";
  cin>>type;
  system("cls");
  if(type == 'n')
    encryption(file_path1,file_path2);
  else if(type == 'd')
    deep_encryption(file_path1,file_path2);
  else{
    cout<<"invalid type"<<endl;
    system("pause");
    system("cls");
    char** argv = 0;
    main(1,argv);
  }
}

void execution_decription(){
  char* file_path1 = (char*) malloc(200);
  cout<<"input the file path to decription: ";
  cin>>file_path1;
  system("cls");
  char* file_path2 = (char*) malloc(200);
  cout<<"input the file path to write the content: ";
  cin>>file_path2;
  system("cls");
  FILE* file = fopen(file_path1,"r");
  char type_mark[4];
  if(file != NULL){
    if(!feof(file)){
      fscanf(file,"%s",type_mark);
    }
    fclose(file);
  }else
    perror("failed to open file");
  if(strcmp(type_mark,"[c]") == 0)
    decription(file_path1,file_path2);
  else if(strcmp(type_mark,"[dc]") == 0)
    deep_decription(file_path1,file_path2);
  else{
    cout<<"incorrect file"<<endl;
    system("pause");
    system("cls");
    char** argv = 0;
    main(1,argv);
  }
}

void execution_encryption_args(char*type,char* file_path1,char* file_path2){
  if(strcmp(type,"--normal") == 0 || strcmp(type,"-n") == 0)
    encryption(file_path1,file_path2);
  else if(strcmp(type,"--deep") == 0 || strcmp(type,"-d") == 0)
    deep_encryption(file_path1,file_path2);
  else
    cout<<"invalid type"<<endl;
}

void execution_decription_args(char* file_path1,char* file_path2){
  FILE* file = fopen(file_path1,"r");
  char type_mark[4];
  if(file != NULL){
    if(!feof(file)){
      fscanf(file,"%s",type_mark);
    }
    fclose(file);
  }else
    perror("failed to open file");
  if(strcmp(type_mark,"[c]") == 0)
    decription(file_path1,file_path2);
  else if(strcmp(type_mark,"[dc]") == 0)
    deep_decription(file_path1,file_path2);
  else
    cout<<"incorrect file"<<endl;
}

void show_content(char* file_path){
  FILE* file = fopen(file_path,"r");
  if(file != NULL){
    char line[10000];
    while(!feof(file)){
      fgets(line,10000,file);
      cout<<line;
    }
  }else
    perror("failed to open file");
  fclose(file);
}

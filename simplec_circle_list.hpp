struct ListcC{
  char information[10000];
  ListcC* next;
};

ListcC* insertC(ListcC* listcc,char* value){
  ListcC* new_list=(ListcC*) malloc(sizeof(ListcC));
  strcpy(new_list->information,value);
  ListcC* auxiliar = listcc;
  if(auxiliar==NULL){
    auxiliar=new_list;
    auxiliar->next=auxiliar;
    return auxiliar;
  }
  while(auxiliar->next!=listcc){
    auxiliar=auxiliar->next;
  }
  auxiliar->next=new_list;
  new_list->next=listcc;
  return listcc;
}
